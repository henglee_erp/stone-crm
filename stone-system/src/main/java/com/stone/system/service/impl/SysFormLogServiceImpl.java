package com.stone.system.service.impl;

import java.util.List;

import com.stone.common.utils.equator.EquatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stone.system.mapper.SysFormLogMapper;
import com.stone.system.domain.SysFormLog;
import com.stone.system.service.ISysFormLogService;

/**
 * 单变更日志记录Service业务层处理
 *
 * @author ruoyi
 * @date 2024-06-07
 */
@Service
public class SysFormLogServiceImpl implements ISysFormLogService
{
    @Autowired
    private SysFormLogMapper sysFormLogMapper;

    /**
     * 查询单变更日志记录
     *
     * @param customerId 单变更日志记录主键
     * @return 单变更日志记录
     */
    @Override
    public SysFormLog selectSysFormLogByCustomerId(String customerId)
    {
        return sysFormLogMapper.selectSysFormLogByCustomerId(customerId);
    }

    /**
     * 查询单变更日志记录列表
     *
     * @param sysFormLog 单变更日志记录
     * @return 单变更日志记录
     */
    @Override
    public List<SysFormLog> selectSysFormLogList(SysFormLog sysFormLog)
    {
        List<SysFormLog> resultList=sysFormLogMapper.selectSysFormLogList(sysFormLog);
        //EquatorUtil.getDifferentFields();
        return resultList;
    }

    /**
     * 新增单变更日志记录
     *
     * @param sysFormLog 单变更日志记录
     * @return 结果
     */
    @Override
    public int insertSysFormLog(SysFormLog sysFormLog)
    {
        return sysFormLogMapper.insertSysFormLog(sysFormLog);
    }

    /**
     * 修改单变更日志记录
     *
     * @param sysFormLog 单变更日志记录
     * @return 结果
     */
    @Override
    public int updateSysFormLog(SysFormLog sysFormLog)
    {
        return sysFormLogMapper.updateSysFormLog(sysFormLog);
    }

    /**
     * 批量删除单变更日志记录
     *
     * @param customerIds 需要删除的单变更日志记录主键
     * @return 结果
     */
    @Override
    public int deleteSysFormLogByCustomerIds(String[] customerIds)
    {
        return sysFormLogMapper.deleteSysFormLogByCustomerIds(customerIds);
    }

    /**
     * 删除单变更日志记录信息
     *
     * @param customerId 单变更日志记录主键
     * @return 结果
     */
    @Override
    public int deleteSysFormLogByCustomerId(String customerId)
    {
        return sysFormLogMapper.deleteSysFormLogByCustomerId(customerId);
    }
}
