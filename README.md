<h4 align="center">基于若依Vue平台搭建的前后端分离的客户关系管理系统</h4>

## 系统简介
STONE-CRM是面向中小企业的客户关系管理系统（CRM）， 以若依（RuoYi）为基础平台开发，
前后端分离架构SpringBoot2.x、ElementUI、Vue、Mybatis-plus、spring security、JWT，
具有方便快捷的定制和二次开发能力！STONE-CRM是一个全开源可应用的优质应用系统！

项目不断迭代中，小红书博主“程序猿与小黄鸭”分享撸代码视频，欢迎关注，欢迎Star，给我一些动力。谢谢！
## CRM功能
1. 客户管理：客户数据录入
2. 线索管理：支持线索转化客户
3. 客户池管理：可配置多个客户池，根据成交等多种条件设置回收规则
4. 商机管理：支持设置多个商机，商机阶段自定义配置
5. 合同管理：合同审批流配置（审批流采用flowable流程引擎，待开发）
6. 回款管理：回款审核、审批流配置（审批流采用flowable流程引擎，待开发）
## 系统功能
1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。 
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。

## 系统演示图

<table>
    <tr>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240423091552.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240426145709.png"/></td>
</tr> 
<tr>
<td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240508093556.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240508093617.png"/></td>
</tr> 
<tr>  
<tr>
<td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240508143745.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240508143750.png"/></td>
</tr> 
<tr>
<tr>
<td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240607093535.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240607093543.png"/></td>
</tr> 

<tr>
<td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240607093549.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240607093556.png"/></td>
</tr> 
<tr>
<td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240607093602.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240426145724.png"/></td>
</tr> 
<tr>        
<td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240423092202.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240423092208.png"/></td>
</tr> 
<tr>        
<td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240423092213.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240423092218.png"/></td>
</tr> 
<tr>        
<td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240423092222.png"/></td>
        <td><img src="https://gitee.com/ren-yuanping/stone-crm/raw/master/images/20240508144029.png"/></td>
</tr>
</table>
